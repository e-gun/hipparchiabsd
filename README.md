You probably need to start here. 

These files should allow you to install an environment with all of the tools to support 
HipparchiaBuilder and HipparchiaServer.

Windows installations are theoretically possible. You just have to manually do what is
outlined in the specific cases: install python, install postgresql, acquire the support
files, configure 'config.ini' and 'config.py' so that they can find everything. 

Linux installations will look a lot like FreeBSD installations.

The FreeBSD walkthrough file will build a firewalled server dedicated to Hipparchia. A certain amount of 
prior knowledge is presupposed.

Please head on over to:

```
00_FreeBSD_initial_setup.txt
```
